# Using client viewer frontend for QIIME2 framework

## Workflow

1. Get a `*.qza` or `*.qzv` file
2. Go to the [viewer primary web page](https://view.qiime2.org/)
3. Upload the file into the viewer
4. View data
5. Export data

## Explained interface examples

### Main page

![main](.gitlab/main.png "main")

### Taxonomic Bar Plots

![taxa-bar-plots](.gitlab/taxa-bar-plots.png "taxa-bar-plots")

### Emperor Ordination

![unweighted_unifrac_emperor](.gitlab/unweighted_unifrac_emperor.png "unweighted_unifrac_emperor")

### Alpha Diversity Boxplots

![faith-pd-group-significance](.gitlab/faith-pd-group-significance.png "faith-pd-group-significance")

# `dada2` data vs `vsearch` data

**TL;DR:** `dada2` output is more precise. 

Otherwise, compare your taxonomy data directly, focusing on the negative control samples. 
Or check [the official documentation](https://docs.qiime2.org/2024.5/tutorials/overview/). 

# Accessing contents of a `qzv` file

Since the `*.qzv` file extension is an abbreviation for `QIIME2 Zipped Visualization` 
and the file itself is compressed using `zlib`, any `*.qzv` file may be opened
directly using regular file archivers, e.g. `7-Zip` or `WinRAR`. 
The tabular data is placed into the `*.csv` or `*.tsv` files. 

# About importing plain text data into Excel

⚠ **<mark><ins>Beware of using Excel</ins> where it is possible!</mark>** ⚠

Learning high-level programming languages (e.g. R, Python or MATLAB) 
and using them for further data analysis instead is highly recommended.

In very limited cases, use this video as a tutorial to import CSV (comma-separated values)
or TSV (tab-separated values) with commas or tabs as column delimiters into Excel.

[![How to Import CSV File Into Excel](https://i3.ytimg.com/vi/ebnNy5yEkvc/hqdefault.jpg)](https://www.youtube.com/watch?v=ebnNy5yEkvc "How to Import CSV File Into Excel")
